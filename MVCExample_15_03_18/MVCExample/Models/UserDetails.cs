﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCExample.Models
{
    public class UserDetails
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Gender { get; set; }
        public string Weight { get; set; }
        public string Height { get; set; }
        public string ShortCV { get; set; }
        public string Mail { get; set; }
        public string PhoneNumber { get; set; }

    }
}