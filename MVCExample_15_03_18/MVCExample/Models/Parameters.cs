﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCExample.Models
{
    public class Parameters
    {
        public int GroupCode { get; set; }
        public string GroupName { get; set; }
    }
}