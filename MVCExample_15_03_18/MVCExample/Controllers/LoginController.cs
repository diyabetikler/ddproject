﻿using MVCExample.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCExample.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Logins model, string Command)
        {
            if (Command=="Giris")
            {
                try
                {
                    using (DiabetEntities db = new DiabetEntities())
                    {

                        var context = db.Login.Where(s => s.UserName == model.UserName && s.Password == model.Password);

                        var emailReq = context.FirstOrDefault<Login>();

                        if (emailReq != null)
                        {
                            return RedirectToAction(controllerName: "Home", actionName: "Index");
                        }
                        else
                        {
                            // "Kullanıcı adı ve şifre hatalıdır" diye gösterilecek
                            return View();
                        }


                    }
                }
                catch (Exception)
                {

                    throw;
                }
            }
            if (Command == "KayitOl")
            {
                return RedirectToAction(controllerName: "Login", actionName: "Register");
            }

            return View();
           
        }

        public ActionResult Register()
        {
            List<SelectListItem> lst = new List<SelectListItem>();
            using (DiabetEntities context = new DiabetEntities())
            {
                List<Parameter> lstP = context.Parameter.ToList();
                foreach (Parameter item in lstP)
                {

                    SelectListItem sItem = new SelectListItem() { Text = item.GroupName, Value = item.GroupCode.ToString() };

                    lst.Add(sItem);

                }

                ViewBag.GetGender = new SelectList((List<SelectListItem>)lst, "Value", "Text");

                return View();
            }
        }

        [HttpPost]
        public ActionResult Register(UserDetails model)
        {

            using (DiabetEntities db = new DiabetEntities())
            {
                Login _login = new Login();
                _login.UserName = model.UserName;
                _login.Password = model.Password;

                db.Entry(_login).State = EntityState.Added;

                db.SaveChanges();
                int id = _login.Id;

                UserDetail _userDetail = new UserDetail();
                _userDetail.UserId = id;
                _userDetail.Name = model.Name;
                _userDetail.Surname = model.Surname;
                _userDetail.Mail = model.Mail;
                _userDetail.PhoneNumber = model.PhoneNumber;
                _userDetail.ShortCV = model.ShortCV;
                _userDetail.Weight = model.Weight;
                _userDetail.Height = model.Height;
                _userDetail.Gender = model.Gender;

                db.UserDetail.Add(_userDetail);
                db.SaveChanges();

            }
            return View();
        }
    }
}